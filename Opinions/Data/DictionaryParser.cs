﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Opinions.Exceptions;

namespace Opinions.Data
{
    public class DictionaryParser
    {
        protected string _filePath;
        protected DictionaryType _dictionaryType;
        public Dictionary<string, List<string>> Dictionary { get; protected set; }

        public DictionaryParser(DictionaryType dictionaryType)
        {
            this._dictionaryType = dictionaryType;
            this._filePath = "./Data/" + dictionaryType.ToString().ToLower() + ".txt";
            this.Dictionary=new Dictionary<string, List<string>>();

            try
            {
                this.Parse();
            }
            catch (FileNotFoundException e)
            {
                throw new DictionaryException(_dictionaryType, DictionaryExceptionReason.ФайлСоСправочикомНеОбнаружен);
            }
            catch
            {
                throw new DictionaryException(_dictionaryType);
            }
        }

        public void Parse()
        {
            using (FileStream stream = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
            {
                StreamReader reader = new StreamReader(stream);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().ToLower();
                    int openBracket;
                    string initialForm = "";

                    try
                    {
                        openBracket = line.IndexOf('(');
                        if (openBracket == -1)
                            throw new DictionaryException(_dictionaryType,
                                                          DictionaryExceptionReason.НевозможноПроанализироватьВариантыФорм);

                        initialForm = string.Concat(line.Where((c, i) => i < openBracket)).Trim().ToUpper();
                    }
                    catch
                    {
                        throw new DictionaryException(_dictionaryType,
                                                      DictionaryExceptionReason.НеОбнаруженаНачальнаяФорма);
                    }

                    try
                    {
                        Dictionary[initialForm] = new List<string>() {initialForm};
                        Dictionary[initialForm].AddRange(line.Substring(openBracket + 1)
                            .Split(new string[] {",", ")"}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(s => s.Trim().ToUpper())
                            .ToList());
                    }
                    catch
                    {
                        throw new DictionaryException(_dictionaryType,
                                                      DictionaryExceptionReason.НевозможноПроанализироватьВариантыФорм);
                    }
                }
                reader.Close();
            }
        }
    
        public bool Contains(string value)
        {
            foreach (string key in Dictionary.Keys)
            {
                if (Dictionary[key].Any(s => s.ToUpper() == value.ToUpper())) return true;
            }
            return false;
        }

        public string GetInitialForm(string form)
        {
            foreach (string key in Dictionary.Keys)
            {
                if (Dictionary[key].Any(s => s.ToUpper() == form.ToUpper())) return key;
            }
            //throw new DictionaryException(_dictionaryType, DictionaryExceptionReason.НеОбнаруженаНачальнаяФорма);
            return form;
        }

        public bool Check(string str)
        {
            foreach (var item in Dictionary.Keys)
            {
                if (Dictionary[item].Any(s => s.ToUpper() == str.ToUpper())) 
                    return true;
            }
            return false;
        }
    }
}
