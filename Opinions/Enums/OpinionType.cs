﻿namespace Opinions.Enums
{
    public enum OpinionType
    {
        Общеутвердительное = 1,
        Общеотрицательное = 2,
        Частноутвердительное = 3,
        Частноотрицательное = 4
    }
}