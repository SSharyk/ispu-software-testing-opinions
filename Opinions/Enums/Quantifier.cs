﻿namespace Opinions.Enums
{
    public enum Quantifier
    {
        All = 0,
        Some,
        No,


        Все = 10,
        
        Некоторые,
        НекоторыеИз,
        НекоторыеИзТехКто,

        НиОдинИзТехКтоЯвляется,
        НиОдинИзТехКто,
        НиОдинИз,
        НиОдин
    }
}