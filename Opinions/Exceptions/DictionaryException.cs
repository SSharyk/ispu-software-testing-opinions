﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Data;
using Opinions.Utilities;

namespace Opinions.Exceptions
{
    internal class DictionaryException : Exception
    {
        public DictionaryException(DictionaryType dictionary,
                                   DictionaryExceptionReason reason = DictionaryExceptionReason.ПричинаНеВыявлена)
            : base(string.Format("Не могу считать данные для объекта: {0}, так как {1}",
                                 dictionary.ToString().Decapitalize(), reason.ToString().Decapitalize()))
        {
        }

        public DictionaryException(string message) :
            base(message)
        {
        }
    }
}