﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Utilities;

namespace Opinions.Exceptions
{
    internal class InvalidOpinionFormatException : Exception
    {
        public FormatExceptionReason Reason { get; protected set; }

        #region Constructors

        public InvalidOpinionFormatException(string message)
            : base(message)
        {
        }

        public InvalidOpinionFormatException(FormatExceptionReason reason = FormatExceptionReason.ФорматЗаписиСужденияНеСоответствуетУказанномуВСпецификации)
            :base(reason.ToString())
        {
            this.Reason = reason;
        }

        #endregion

        public override string Message
        {
            get { return Reason.ToString().Decapitalize(); }
        }
    }
}