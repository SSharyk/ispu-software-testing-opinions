﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Enums;
using Opinions.Exceptions;
using Opinions.Solves;
using Opinions.Utilities;
using System.IO;

namespace Opinions
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // from input file
            try
            {
                using (FileStream stream = new FileStream("input.txt", FileMode.Open, FileAccess.Read))
                {
                    StreamReader reader = new StreamReader(stream);
                    while (!reader.EndOfStream)
                    {
                        string s = reader.ReadLine();
                        MakeAllOperationsForCommon(s);
                        LogHelper.Empty(3);
                    }
                    stream.Close();
                    reader.Close();
                }
                Console.ReadKey();
                return;
            }
            catch (FileNotFoundException e)
            {
                LogHelper.Log("Файл с исходными записями не обнаружен. Пожалуйста, выполните ввод с клавиатуры",
                              MessageType.Error);
            }
            catch (InvalidOpinionFormatException e)
            {
                LogHelper.Log(e.Reason.ToString().Decapitalize(), MessageType.Error);
                Console.ReadKey();
                return;
            }
            catch (Exception e)
            {
                LogHelper.Log(e.Message, MessageType.Error);
                Console.ReadKey();
                return;
            }

            // from command-line parameters
            try
            {
                string s = "";
                do
                {
                    LogHelper.Log("Пожалуйста, введите суждение. Для завершения работы программы введите пустую строку");
                    s = Console.ReadLine();
                    if (s.Length > 0)
                        MakeAllOperationsForCommon(s);

                    LogHelper.Empty(3);
                } while (s.Length > 0);
            }
            catch (Exception e)
            {
                /// TODO: impement log
                LogHelper.Log(e.Message, MessageType.Error);
            }
        }

        private static void MakeAllOperationsForCommon(string s)
        {
            /// TODO: add improved polymorphism for common and part opinions
            Opinion opinion = new Opinion(s);
            LogHelper.Log("Входные данные: " + opinion.Expression);

            OpinionType type = opinion.OpinionType;
            LogHelper.Log("Тип: " + type.ToString().ToUpper());

            switch (type)
            {
                case OpinionType.Общеутвердительное:
                case OpinionType.Общеотрицательное:
                    CommonOpinion cOpinion = new CommonOpinion(opinion);
                    LogHelper.Log("Превращение: " + cOpinion.Transformation().CreateExpression());
                    LogHelper.Log("Обращение: " + cOpinion.Convertion().CreateExpression());
                    LogHelper.Log("Противопоставление: " + cOpinion.Contradisctinction().CreateExpression());
                    break;
                case OpinionType.Частноутвердительное:
                    PartOpinion pOpinion = new PartOpinion(opinion);
                    LogHelper.Log("Превращение: " + pOpinion.Transformation().CreateExpression());
                    LogHelper.Log("Обращение: " + pOpinion.Convertion().CreateExpression());
                    break;
                case OpinionType.Частноотрицательное:
                    PartOpinion pOpinion1 = new PartOpinion(opinion);
                    LogHelper.Log("Превращение: " + pOpinion1.Transformation().CreateExpression());
                    LogHelper.Log("Противопоставление: " + pOpinion1.Contradisctinction().CreateExpression());
                    break;
                default:
                    throw new InvalidOpinionFormatException();
            }
        }
    }
}