﻿using System;
using System.Collections.Generic;
using Opinions.Enums;
using Opinions.Utilities;

namespace Opinions.Solves
{
    public class CommonOpinion : Opinion, IOpinionOperations<CommonOpinion>
    {
        #region Constructors

        /// <summary>
        /// Creates new instance of <see cref="CommonOpinion"/> object
        /// </summary>
        /// <param name="expression">Opinion string expression</param>
        public CommonOpinion(string expression)
            : base(expression)
        {
        }

        /// <summary>
        /// Make a copy of the <see cref="CommonOpinion"/> object
        /// </summary>
        /// <param name="origin">The <see cref="CommonOpinion"/> instance to copy</param>
        public CommonOpinion(Opinion origin)
            : base(origin)
        {
        }

        #endregion

        #region Operations on opinions

        /// <summary>
        /// Отрицание
        /// </summary>
        /// <returns>New instance of <see cref="CommonOpinion"/> class that is negated to the current</returns>
        public CommonOpinion Negation()
        {
            return new CommonOpinion(this)
                {
                    IsLinkPositive = !this.IsLinkPositive
                };
        }

        /// <summary>
        /// Превращение
        /// </summary>
        /// <returns>New instance of <see cref="CommonOpinion"/> class that is transformed to the current</returns>
        public CommonOpinion Transformation()
        {
            var copy = new CommonOpinion(this);
            
            copy.IsPredicatePositive = !this.IsPredicatePositive;
            copy.IsLinkPositive = !this.IsLinkPositive;
            copy.Quantifier = (this.OpinionType == OpinionType.Общеутвердительное)
                                  ? Quantifier.Element(LocalizationHelper.GetString("QNo").Capitalize())
                                  : Quantifier.Element(LocalizationHelper.GetString("QAll").Capitalize());

            return copy;
        }

        /// <summary>
        /// Обращение
        /// </summary>
        /// <returns>New instance of <see cref="CommonOpinion"/> class that is converted to the current</returns>
        public CommonOpinion Convertion()
        {
            var copy = new CommonOpinion(this)
                {
                    Subject = this.Predicate,
                    Predicate = this.Subject
                };

            if (OpinionType == OpinionType.Общеутвердительное)
            {
                copy.Quantifier = Quantifier.Element(LocalizationHelper.GetString(Variables.QSome).Capitalize());
            }
            if (!LocalizationHelper.GetAllowedStringsFor("Link").Contains(Link))
            {
                copy.Quantifier = (OpinionType == OpinionType.Общеутвердительное)
                                      ? Quantifier.НекоторыеИзТехКто
                                      : Quantifier.НиОдинИзТехКто;
                copy.Subject = Link + " " + copy.Subject;
                copy.Link = " ";
            }

            return copy;
        }

        /// <summary>
        /// Противопоставление
        /// </summary>
        /// <returns>New instance of <see cref="CommonOpinion"/> class that is contradistincted to the current</returns>
        public CommonOpinion Contradisctinction(bool isViaMethodsChain = false)
        {
            if (isViaMethodsChain)
            {
                return this.Transformation().Convertion();
            }
            var neg = new CommonOpinion(this)
                {
                    IsPredicatePositive = !this.IsPredicatePositive,
                };
            if (!LocalizationHelper.GetAllowedStringsFor("Link").Contains(Link))
            {
                neg.Link = " ";
            }

            var copy = new CommonOpinion(neg)
                {
                    Subject =
                        ((IsPredicatePositive) ? LocalizationHelper.GetString(Variables.Not) : "") +
                        " " + Link + " " + this.Predicate,
                    IsLinkPositive = !this.IsLinkPositive,
                    Predicate = neg.Subject,
                    IsPredicatePositive = (!neg.IsPredicatePositive ^ !this.IsPredicatePositive),

                    Quantifier = (this.OpinionType == OpinionType.Общеутвердительное)
                                     ? Quantifier.Element(LocalizationHelper.GetString(Variables.QNo).Capitalize())
                                     : Quantifier.Element(LocalizationHelper.GetString(Variables.QSome).Capitalize())
                };

            return copy;
        }

        
        #endregion
    }
}