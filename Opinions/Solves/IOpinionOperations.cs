﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Opinions.Solves
{
    /// <summary>
    /// Contains methos for operations on opinions
    /// </summary>
    interface IOpinionOperations<O> 
        where O : class
    {
        /// <summary>
        /// Отрицание
        /// </summary>
        /// <returns>New instance of <see cref="O"/> class that is negated to the current</returns>
        O Negation();

        /// <summary>
        /// Превращение
        /// </summary>
        /// <returns>New instance of <see cref="O"/> class that is transformed to the current</returns>
        O Transformation();

        /// <summary>
        /// Обращение
        /// </summary>
        /// <returns>New instance of <see cref="O"/> class that is converted to the current</returns>
        O Convertion();

        /// <summary>
        /// Противопоставление
        /// </summary>
        /// <returns>New instance of <see cref="O"/> class that is contradistincted to the current</returns>
        O Contradisctinction(bool isViaMethodsChain = false);
    }
}
