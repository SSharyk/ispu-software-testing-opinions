﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Opinions.Data;
using Opinions.Enums;
using Opinions.Exceptions;
using Opinions.Utilities;

namespace Opinions.Solves
{
    public class Opinion
    {
        #region Constructors

        /// <summary>
        /// Creates new instance of <see cref="Opinion"/> object
        /// </summary>
        /// <param name="expression">Opinion string expression</param>
        public Opinion(string expression)
        {
            this.Expression = expression;
            this.ParseExpression();
        }
        /// <summary>
        /// Creates a copy of the <see cref="Opinion"/> object
        /// </summary>
        /// <param name="origin">The <see cref="Opinion"/> instance to copy</param>
        public Opinion(Opinion origin)
        {
            this.Quantifier = origin.Quantifier;
            this.Subject = origin.Subject;
            this.Link = origin.Link;
            this.Predicate = origin.Predicate;
            this.IsLinkPositive = origin.IsLinkPositive;
            this.IsPredicatePositive = origin.IsPredicatePositive;
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets or sets the string that represents opinion on native language (suppose, English)
        /// Should be formatted: &lt;Quantifier&gt; (&lt;Subject&gt;) &lt;Link&gt; [&lt;Predicate&gt;]
        /// </summary>
        public string Expression { get; protected set; }

        /// <summary>
        /// Gets or sets the quantifier for subjects. May be: any, exsists, etc.
        /// </summary>
        public Quantifier Quantifier { get; protected set; }

        /// <summary>
        /// Gets or sets the subject of the opinion
        /// </summary>
        public string Subject { get; protected set; }

        /// <summary>
        /// Gets or sets the link of the opinion
        /// </summary>
        public string Link { get; protected set; }

        /// <summary>
        /// Gets or sets the predicate of the opinion
        /// </summary>
        public string Predicate { get; protected set; }

        /// <summary>
        /// Gets or sets the type of the opinion's link (either positive or negative)
        /// </summary>
        public bool IsLinkPositive { get; protected set; }

        /// <summary>
        /// Gets or sets the type of the opinion's predicate (either positive or negative)
        /// </summary>
        public bool IsPredicatePositive { get; protected set; }

        /// <summary>
        /// Returns classification item of the opinion
        /// </summary>
        public OpinionType OpinionType
        {
            get
            {
                switch (Quantifier)
                {
                    case Quantifier.All:
                    case Quantifier.Все:
                        {
                            if (IsLinkPositive)
                                return OpinionType.Общеутвердительное;
                            throw new InvalidOpinionFormatException(FormatExceptionReason.ОбщеутвердительноеСуждениеДолжноИметьПоложительнуюСвязку);
                        }
                        //return (!IsPredicatePositive ^ IsLinkPositive
                    //            ? OpinionType.CommonAffirmative
                    //            : OpinionType.CommonNegative);

                    case Quantifier.Some:
                    case Quantifier.Некоторые:
                    case Quantifier.НекоторыеИз:
                    case  Quantifier.НекоторыеИзТехКто:
                        {
                            if (IsLinkPositive && IsPredicatePositive)
                                return OpinionType.Частноутвердительное;
                            else if (!IsLinkPositive && IsPredicatePositive)
                                return OpinionType.Частноотрицательное;
                            throw new InvalidOpinionFormatException(FormatExceptionReason.ТипСужденияНеМожетБытьОпределен);
                        }
                        //return (!IsPredicatePositive ^ IsLinkPositive
                        //            ? OpinionType.Частноутвердительное
                        //            : OpinionType.Частноотрицательное);

                    case Quantifier.No:
                    case Quantifier.НиОдинИзТехКтоЯвляется:
                    case Quantifier.НиОдинИзТехКто:
                    case Quantifier.НиОдинИз:
                    case Quantifier.НиОдин:
                        {
                            if (!IsLinkPositive)
                            //if (!IsLinkPositive && IsPredicatePositive)
                                return OpinionType.Общеотрицательное;
                            throw new InvalidOpinionFormatException(FormatExceptionReason.ТипСужденияНеМожетБытьОпределен);
                        }
                    //return (IsPredicatePositive ^ IsLinkPositive
                    //            ? OpinionType.CommonAffirmative
                    //            : OpinionType.CommonNegative);

                    default:
                        throw new ArgumentOutOfRangeException();

                }
            }
        }

        #endregion

        #region Create/parse string expression
        /// <summary>
        /// Parses separate parts of the string expression
        /// </summary>
        public void ParseExpressionViaRegex()
        {
            /// TODO: add all test cases for invalid format
            if (Expression == null)
                throw new ArgumentNullException(paramName: "Expression");

            try
            {
                string allowed = "[a-zA-Zа-яА-Я ]+";
                string pattern = string.Format("({0})[(]({0})[)]({0})[[]({0})[]]", allowed);
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                if (!regex.IsMatch(Expression))
                    throw new InvalidOpinionFormatException();

                /// TODO: implement EnumHelper
                try
                {
                    this.Quantifier = Quantifier.Element(regex.Matches(Expression)[0].Groups[1].Value.Capitalize());
                    if (string.IsNullOrEmpty(Quantifier.ToString()))
                        throw new InvalidOpinionFormatException(FormatExceptionReason.КванторНеОпределен);
                }
                catch
                {
                    throw new InvalidOpinionFormatException(FormatExceptionReason.КванторНеОпределен);
                }

                this.Subject = regex.Matches(Expression)[0].Groups[2].Value;

                this.Link = regex.Matches(Expression)[0].Groups[3].Value.Trim();
                if (Link == null || Link.Length == 0)
                    //if (!LocalizationHelper.GetAllowedStrings(Variables.Link).Contains(Link))
                    throw new InvalidOpinionFormatException(FormatExceptionReason.СвязкаНеОпределена);

                this.Predicate = regex.Matches(Expression)[0].Groups[4].Value;

                if (Link.EndsWith(LocalizationHelper.GetString(Variables.Not)) ||
                    Link.StartsWith(LocalizationHelper.GetString(Variables.Not)))
                {
                    this.IsLinkPositive = false;
                    Link = Link.Trim(LocalizationHelper.GetString(Variables.Not).ToCharArray()).Trim();
                }
                else
                {
                    this.IsLinkPositive = true;
                }

                if (Predicate.StartsWith(LocalizationHelper.GetString(Variables.Not)) ||
                    Predicate.EndsWith(LocalizationHelper.GetString(Variables.Not)))
                {
                    this.IsPredicatePositive = false;
                    Predicate = Predicate.Trim(LocalizationHelper.GetString(Variables.Not).ToCharArray()).Trim();
                }
                else
                {
                    this.IsPredicatePositive = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ParseExpression()
        {
            if (Expression == null)
                throw new ArgumentNullException(paramName: "Expression");
            Expression = Expression.ToUpper();

            var words =
                Expression.Split(new string[] {" ", ","}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(word => word.ToUpper()).ToList();

            var subjParser = new DictionaryParser(DictionaryType.Субъект);

            int w = 0;

            do
            {
                foreach (string initialForm in subjParser.Dictionary.Keys)
                {
                    if (initialForm == words[w] || subjParser.Dictionary[initialForm].Contains(words[w]))
                    {
                        Subject = words[w];
                        break;
                    }
                }
                w++;
            } while (w < words.Count && Subject == null);

            if (Subject == null) throw new InvalidOpinionFormatException(FormatExceptionReason.СубъектНеОпределен);

            string quantr = string.Join(" ", words.Take(w - 1));
            Quantifier = Quantifier.Element(quantr); //Если не обнаружен квантор то вывалится exception внутри метода

            //Link = words[w];

            Link = words.Skip(w).Take(1).DefaultIfEmpty("").First();
            //Такая длинная запись позволяет избежать проблем выхода за границу массива из-за индекса W 
            //и из-за использования defaultIfEmpty вывести адекватное сообщение об ошибке
            if (Link == LocalizationHelper.GetString(Variables.Not).ToUpper())
            {
                w++;
                IsLinkPositive = false;
                //Link = words[w];
                Link = words.Skip(w).Take(1).DefaultIfEmpty("").First();
            }
            else
            {
                IsLinkPositive = true;
            }

            if (!new DictionaryParser(DictionaryType.Связка).Check(this.Link))
            {
                throw new InvalidOpinionFormatException(FormatExceptionReason.СвязкаНеОпределена);
            }

            w++;

            Predicate = words.Skip(w).Take(1).DefaultIfEmpty("").First();
            if (Predicate == LocalizationHelper.GetString(Variables.Not).ToUpper())
            {
                w++;
                IsPredicatePositive = false;
            }
            else
            {
                IsPredicatePositive = true;
            }

            Predicate = string.Join(" ", words.Skip(w).Take(words.Count - w));
                    
            if (!new DictionaryParser(DictionaryType.Предикат).Check(this.Predicate))
            {
                throw new InvalidOpinionFormatException(FormatExceptionReason.ПредикатНеОпределен);
            }
        }

        /// <summary>
        /// Creates expression from known parts
        /// </summary>
        /// <returns>Expression formatted as an expression</returns>
        public string CreateExpression()
        {
            /// TODO: re-implement with grammar rules
            /// TODO: override ToString() object method equal this method
            this.Expression = this.ToString();
            return this.Expression.Remove(new string[] {"(", ")", "[", "]"}).RemoveDoubleSymbols(' ');
        }

        #endregion

        public override string ToString()
        {
            //return string.Format("{0} ({1}) {2}{3} [{4}{5}]",
            //                     this.Quantifier.ToString().Decapitalize().ToUpper(),
            //                     new DictionaryParser(DictionaryType.Субъект).GetInitialForm(this.Subject),
            //                     (this.IsLinkPositive) ? "" : (LocalizationHelper.GetString(Variables.Not) + " "),
            //                     new DictionaryParser(DictionaryType.Связка).GetInitialForm(this.Link),
            //                     (this.IsPredicatePositive) ? "" : (LocalizationHelper.GetString(Variables.Not) + " "),
            //                     new DictionaryParser(DictionaryType.Предикат).GetInitialForm(this.Predicate));

            return string.Format("{0} ({1}) {2}{3} [{4}{5}]",
                                 this.Quantifier.ToString().Decapitalize().ToUpper(),
                                 this.Subject.ToUpper(),
                                 (this.IsLinkPositive) ? "" : (LocalizationHelper.GetString(Variables.Not) + " "),
                                 this.Link.ToUpper(),
                                 (this.IsPredicatePositive) ? "" : (LocalizationHelper.GetString(Variables.Not) + " "),
                                 this.Predicate.ToUpper());
        }
    }
}
