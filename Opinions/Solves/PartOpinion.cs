﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Utilities;
using Opinions.Enums;

namespace Opinions.Solves
{
    public class PartOpinion : Opinion, IOpinionOperations<PartOpinion>
    {
        #region Constructors
        /// <summary>
        /// Creates new instance of <see cref="PartOpinion"/> object
        /// </summary>
        /// <param name="expression">Opinion string expression</param>
        public PartOpinion(string expression)
            : base(expression)
        {
        }

        /// <summary>
        /// Make a copy of the <see cref="PartOpinion"/> object
        /// </summary>
        /// <param name="origin">The <see cref="PartOpinion"/> instance to copy</param>
        public PartOpinion(Opinion origin)
            : base(origin)
        {
        }
        #endregion

        #region Operations on opinions
        /// <summary>
        /// Отрицание
        /// </summary>
        /// <returns>New instance of <see cref="PartOpinion"/> class that is negated to the current</returns>
        public PartOpinion Negation()
        {
            return new PartOpinion(this)
            {
                IsLinkPositive = !this.IsLinkPositive
            };
        }

        /// <summary>
        /// Превращение
        /// </summary>
        /// <returns>New instance of <see cref="PartOpinion"/> class that is transformed to the current</returns>
        public PartOpinion Transformation()
        {
            var copy = new PartOpinion(this);
            //if (OpinionType == OpinionType.Частноутвердительное)
            //{
                copy.IsPredicatePositive = !this.IsPredicatePositive;
                copy.IsLinkPositive = !this.IsLinkPositive;
            //}
            //if (OpinionType == OpinionType.Частноотрицательное)
            //{
            //    copy.IsPredicatePositive = !this.IsPredicatePositive;
            //}
            return copy;
        }

        /// <summary>
        /// Обращение
        /// </summary>
        /// <returns>New instance of <see cref="PartOpinion"/> class that is converted to the current</returns>
        public PartOpinion Convertion()
        {
            //Только для частноутвердительных
            var copy = new PartOpinion(this)
            {
                Subject = this.Predicate,
                Predicate = this.Subject
            };
            if (OpinionType == OpinionType.Частноутвердительное)
            {
                copy.Quantifier = Quantifier.Element(LocalizationHelper.GetString(Variables.QSome).Capitalize());
            }
            return copy;
        }

        /// <summary>
        /// Противопоставление
        /// </summary>
        /// <returns>New instance of <see cref="PartOpinion"/> class that is contradistincted to the current</returns>
        public PartOpinion Contradisctinction(bool isViaMethodsChain = false)
        {
            //Только для частноотрицательных
            var copy = new PartOpinion(this)
            {
                //Subject = ((IsPredicatePositive) ? LocalizationHelper.GetString(Variables.Not) : "") +
                //    " " + Link + " " + this.Predicate,
                //IsLinkPositive = !this.IsLinkPositive,
                //Predicate = this.Subject,
                //Link = " ",
                //Quantifier = Quantifier.Element(LocalizationHelper.GetString(Variables.QSome).Capitalize())

                Quantifier = Quantifier.Element(LocalizationHelper.GetString(Variables.QSome).Capitalize()),
                Predicate = this.Subject,
                Subject = ((IsPredicatePositive) ? LocalizationHelper.GetString(Variables.Not) + " " : "") + this.Predicate,
                IsLinkPositive = !this.IsLinkPositive
            };
            return copy;
        }
        #endregion
    }
}
