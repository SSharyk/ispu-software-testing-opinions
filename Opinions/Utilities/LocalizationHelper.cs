﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Enums;

namespace Opinions.Utilities
{
    public enum Languages
    {
        En,
        Ru
    }

    public enum Variables
    {
        Not,
        Link,
        QNo,
        QAll,
        QSome
    }

    public static class LocalizationHelper
    {
        public static Languages Locale { get; set; }
        private static readonly Dictionary<Languages, Dictionary<Variables, List<string>>> _values;

        static LocalizationHelper()
        {
            Locale = (Languages) Enum.Parse(typeof (Languages), Opinions.Properties.Settings.Default.DefaultLanguage);

            _values = new Dictionary<Languages, Dictionary<Variables, List<string>>>();
            _values[Languages.Ru] = new Dictionary<Variables, List<string>>()
                {
                    {Variables.Not, new List<string>() {"не"}},
                    {Variables.Link, new List<string>() {"-", "не"}},
                    {
                        Variables.QNo, new List<string>()
                            {
                                Quantifier.НиОдинИзТехКто.ToString().Decapitalize(),
                                Quantifier.НиОдинИзТехКтоЯвляется.ToString().Decapitalize(),
                                Quantifier.НиОдинИз.ToString().Decapitalize(),
                            }
                    },
                    {Variables.QAll, new List<string>() {Quantifier.Все.ToString()}},
                    {
                        Variables.QSome, new List<string>()
                            {
                                Quantifier.НекоторыеИзТехКто.ToString().Decapitalize(),
                                Quantifier.Некоторые.ToString().Decapitalize(),
                                Quantifier.НекоторыеИз.ToString().Decapitalize()
                            }
                    }
                };
            _values[Languages.En] = new Dictionary<Variables, List<string>>()
                {
                    {Variables.Not, new List<string>() {"not"}},
                    {Variables.Link, new List<string>() {"is", "are", "am", "is not", "are not", "am not", "'s not", "'re not"}},
                    {Variables.QNo, new List<string>() {Quantifier.No.ToString()}},
                    {Variables.QAll, new List<string>() {Quantifier.All.ToString()}},
                    {Variables.QSome, new List<string>() {Quantifier.Some.ToString()}}
                };
        }

        public static string GetString(Variables v)
        {
            try
            {
                var x = _values[Locale][v];
                return x.FirstOrDefault().Decapitalize().ToUpper();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string GetString(string name)
        {
            try
            {
                var x = _values[Locale][(Variables) Enum.Parse(typeof (Variables), name)];
                return x.FirstOrDefault().Decapitalize().ToUpper();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static List<string> GetAllowedStringsFor(Variables v)
        {
            try
            {
                return _values[Locale][v].Select(s => s.Decapitalize().ToUpper()).ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public static List<string> GetAllowedStringsFor(string name)
        {
            try
            {
                return _values[Locale][(Variables)Enum.Parse(typeof(Variables), name)].Select(s=>s.Decapitalize().ToUpper()).ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }
    }
}
