﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Opinions.Utilities
{
    public enum MessageType
    {
        Info,
        Warning,
        Error
    }

    public static class LogHelper
    {
        private static readonly FileStream _stream = new FileStream("../../opinions.log", FileMode.OpenOrCreate,
                                                                    FileAccess.ReadWrite);

        static  LogHelper()
        {
            using (FileStream stream = new FileStream("output.txt", FileMode.Create, FileAccess.Write))
            {
                stream.Close();
            }
        }

        public static void Log(string message, MessageType messageType = MessageType.Info)
        {
            XElement msg =
                XElement.Parse(string.Format("<{0} date=\"{1}\">{2}</{0}>",
                                             messageType.ToString(), DateTime.Now, message));

            string consoleMsg = string.Format("{0} > {1}", messageType.ToString()[0], message);
            Console.WriteLine(consoleMsg);

            using (FileStream stream = new FileStream("output.txt", FileMode.Open, FileAccess.Write))
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.BaseStream.Seek(0, SeekOrigin.End);
                writer.WriteLine(consoleMsg);
                writer.Close();
            }

            //using (XmlTextWriter writer = new XmlTextWriter(_stream, Encoding.Default))
            //{
            //    writer.BaseStream.Seek(0, SeekOrigin.End);
            //    msg.WriteTo(writer);
            //    writer.WriteString("\r\n");
            //    writer.BaseStream.Close();
            //}
        }

        public static void Empty(int count=1)
        {
            using (FileStream stream = new FileStream("output.txt", FileMode.Open, FileAccess.Write))
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.BaseStream.Seek(0, SeekOrigin.End);

                while (count > 0)
                {
                    Console.WriteLine();
                    writer.WriteLine();
                    count--;
                }

                writer.Close();
            }
        }
    }
}