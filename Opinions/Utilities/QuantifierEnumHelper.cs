﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Opinions.Data;
using Opinions.Enums;
using Opinions.Exceptions;

namespace Opinions.Utilities
{
    /// TODO: make it universal
    public static class QuantifierEnumHelper
    {
        public static Quantifier Element(this Quantifier quantifier, string value)
        {
            if (!string.IsNullOrWhiteSpace(value)) //ДОБАВЛЕНО!!!
            {
                var names = Enum.GetNames(typeof(Quantifier));
                foreach (string name in names)
                {
                    if (name.ToUpper() == value.Capitalize().ToUpper())
                        return (Quantifier)Enum.Parse(typeof(Quantifier), name);
                }
            }
            throw new DictionaryException(DictionaryType.Квантор, DictionaryExceptionReason.НеОбнаруженаТребуемаяФорма);
        }
    }
}
