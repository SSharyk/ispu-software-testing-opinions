﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Opinions.Utilities
{
    public static class StringExtensions
    {
        private static readonly char[] _delimeters = new char[] {' ', '(', ')', '[', ']'};
        //private static readonly char[] _delimeters = new char[] {' '};

        /// <summary>
        /// Removes spaces and converts first letters of each word
        /// </summary>
        /// <param name="s">Input string</param>
        /// <returns>New string which is the only word (with uppercase letters inside the word)</returns>
        public static string Capitalize(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            string res = "";
            res += s[0];

            for (int i = 1; i < s.Length; i++)
            {
                res += (_delimeters.Contains(s[i - 1]) ? Char.ToUpper(s[i]) : s[i]);
            }
            res = res.Remove(" ");

            return res;
        }

        /// <summary>
        /// Adds spaces and converts first letters of each word
        /// </summary>
        /// <param name="s">Input string</param>
        /// <returns>Correct case string</returns>
        public static string Decapitalize(this string s)
        {
            string res = "";
            res += s[0];

            for (int i = 1; i < s.Length; i++)
            {
                res += ((Char.ToUpper(s[i]) == s[i]) ? (" " + Char.ToLower(s[i]).ToString()) : s[i].ToString());
                //res += (_delimeters.Contains(s[i - 1]) ? Char.ToLower(s[i]) : s[i]);
            }

            return res;
        }

        /// <summary>
        /// Removes specfic substring from string
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="c">Specific substring</param>
        /// <returns>New string in which all occurrences of a specific substring are replaced with empty substring</returns>
        public static string Remove(this string s, string c)
        {
            return s.Replace(c, @"");
        }

        /// <summary>
        /// Removes specfic substrings from string
        /// </summary>
        /// <param name="s">Input string</param>
        /// <param name="chars">Enumeration of specific substrings</param>
        /// <returns>New string in which all occurrences of a specific substring are replaced with empty substring</returns>
        public static string Remove(this string s, IEnumerable<string> chars)
        {
            return chars.Aggregate(s, (current, c) => current.Remove(c));
        }

        public static string RemoveDoubleSymbols(this string s, char c)
        {
            string res = s;
            string dbl = (c.ToString() + c.ToString());
            while (res.Contains(dbl))
            {
                res = res.Replace(dbl, c.ToString());
            }
            return res;
        }
    }
}
