# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###

* This project is implementation of my university task on "Software Testing" discipline
* Verion 1.0

### How do I get set up? ###

* Just build and run the project (I use VS 2010)
* .NET Framework 4.0
* Dependencies are described in solution files
* No database uses
* To run test, run ReSharper

### Contribution guidelines ###

* Test should be writed by me only because it is *my own* task for the term
* If you have any suggestions about code style, please notify me with Notes.txt
WARNING! Please, make *your own* changes if *your own* branch!
* Notes.txt may be replaced with XML-file

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact